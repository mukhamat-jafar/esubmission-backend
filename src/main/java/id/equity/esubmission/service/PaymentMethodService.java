package id.equity.esubmission.service;

import id.equity.esubmission.config.error.NotFoundException;
import id.equity.esubmission.dto.PaymentMethod.CreatePaymentMethod;
import id.equity.esubmission.model.PaymentMethod;
import id.equity.esubmission.repository.PaymentMethodRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class PaymentMethodService {
    @Autowired
    private PaymentMethodRepository paymentMethodRepository;

    @Autowired
    private ModelMapper modelMapper;

    //List Payment Method
    public ResponseEntity<List<PaymentMethod>> listPaymentMethod() {
        List<PaymentMethod> listPaymentMethods = paymentMethodRepository.findAll();
        Type targetType = new TypeToken<List<PaymentMethod>>() {}.getType();
        List<PaymentMethod> response = modelMapper.map(listPaymentMethods, targetType);

        return ResponseEntity.ok(response);
    }

    //List Payment Method By Id
    public ResponseEntity<PaymentMethod> getpaymentMethodById(Long id) {
        PaymentMethod paymentMethod = paymentMethodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Method id " + id + " is not exist"));
        PaymentMethod response = modelMapper.map(paymentMethod, PaymentMethod.class);

        return ResponseEntity.ok(response);
    }

    //Create Payment Method
    public ResponseEntity<PaymentMethod> addPaymentMethod(CreatePaymentMethod newPaymentMethod) {

        PaymentMethod paymentMethod = modelMapper.map(newPaymentMethod, PaymentMethod.class);
        paymentMethod.setPaymentMethodCode(newPaymentMethod.getPaymentMethodCode());
        paymentMethod.setDescription(newPaymentMethod.getDescription());
        paymentMethod.setActive(true);

        paymentMethodRepository.save(paymentMethod);
        PaymentMethod response = modelMapper.map(paymentMethod, PaymentMethod.class);

        return ResponseEntity.ok(response);
    }

    //Edit PaymentMethod
    public ResponseEntity<PaymentMethod> editPaymentMethod(CreatePaymentMethod updatePaymentMethod, Long id) {
        PaymentMethod paymentMethod = paymentMethodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Method id " + id + " is not exist"));

        //manual map
        paymentMethod.setPaymentMethodCode(updatePaymentMethod.getPaymentMethodCode());
        paymentMethod.setDescription(updatePaymentMethod.getDescription());
        paymentMethod.setActive(true);

        paymentMethodRepository.save(paymentMethod);
        PaymentMethod response = modelMapper.map(paymentMethod, PaymentMethod.class);

        return ResponseEntity.ok(response);
    }

    //Delete PaymentMethod
    public ResponseEntity<PaymentMethod> deletePaymentMethod(Long id) {
        PaymentMethod paymentMethod = paymentMethodRepository.findById(id).orElseThrow(()-> new NotFoundException("Payment Method id " + id + " is not exist"));

        paymentMethodRepository.deleteById(id);
        PaymentMethod response = modelMapper.map(paymentMethod, PaymentMethod.class);

        return ResponseEntity.ok(response);
    }
}
